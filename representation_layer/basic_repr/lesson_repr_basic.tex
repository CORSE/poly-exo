\documentclass[a4paper, 11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

%\usepackage{hyperref}
\voffset -1.5cm
\hoffset 0.0cm
\textheight 23cm
\textwidth 16cm
\topmargin 0.0cm
\oddsidemargin 0.0cm
\evensidemargin 0.0cm

\usepackage{epsfig}  
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}

\usepackage{tikz}
\usepackage{array}


\usepackage{listings}
\lstdefinestyle{myC}{
  basicstyle=\footnotesize,
  language=C++,
  tabsize=2,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{gray}\ttfamily, 
  morecomment=[l][\color{magenta}]{\#},
  mathescape=true
}


\title{Program representation with polyhedron and affine function}
\author{Guillaume Iooss}
\date{}


\begin{document}

\maketitle

This document presents how mathematical objects are used to represent elements of a polyhedral program.

We will first describe the class of program that can be represented precisely by using these mathematical objects (\emph{polyhedral programs}).
Then, we will define and show how to represent (as union of polyhedra or affine functions) the concepts of iteration domain, access function, affine scheduling and memory mapping.


% == Polyhedral program (defined through Scop)
\paragraph{Polyhedral program class (Scop definition)}
We consider a class of program called \emph{polyhedral programs}, which are intuitively programs whose characteristics can be represented using union of polyhedra and affine functions.
% TODO: "and piecewise affine function." ??? (issue: piecewise not introduced in the previous dependence)

It can be defined formally through the notion of \emph{Affine Control Loops} (also called Static Control Parts, SCoP). This is a combination of if conditions, for loops and statements which manipulates variable and arrays, such as:
\begin{itemize}
	\item All loop bounds are affine bounds of the surrounding indexes and parameters.
	\item All if conditions must be affine conditions of the surrounding indexes and parameters.
	\item All array accesses are affine expression of the surrounding indexes and parameters.
\end{itemize}

\vspace{0.3cm}
\noindent\emph{Examples:} Here is the basic implementation of a matrix multiplication, where $N$ is a program parameter:
\begin{lstlisting}[style=myC]
for (int i=0; i<N; i++)
  for (int j=0; j<N; j++)
    for (int k=0; k<N; k++)
      C[i,j] = C[i,j] + A[i,k] * B[k,j];  // S
\end{lstlisting}

Here is a solver for a linear triangular equation system, where $N$ is a program parameter:
\begin{lstlisting}[style=myC]
for (int i=0; i<N; i++) {
  temp = 0.0;                    // S0
  for (int k=0; k<i; k++)
    temp = temp + L[i,k] * x[k]; // S1
  x[i] = (b[i] - temp) / L[i,i]; // S2
}
\end{lstlisting}


% == Perfectly nested, imperfectly nested (while we are at it)
\paragraph{Perfectly/imperfectly nested}
Notice that the first example is \emph{perfectly nested}: it is composed of a single statement, surrounded by loops.
This is not the case with the second example, which is \emph{imperfectly nested}: it contains multiple statements that are placed at different levels of the loops.



% == Iteration vector & domain
\paragraph{Iteration vector and iteration domain}
An \emph{iteration vector} is a vector of the surrounding loop indices of a statement. It is used to identify a specific instance of this statement.

\vspace{0.3cm}
\noindent\emph{Example:} For the statements of the examples introduced previously:
\begin{itemize}
	\item $(i,j,k) = (1, 3, 2)$ is an iteration vector for statement \texttt{S}.
	\item $(i) = (N-1)$ is an iteration vector for statement \texttt{S2}.
	\item $(i,k) = (2, 1)$ is an iteration vector for statement \texttt{S1}.
\end{itemize}

The \emph{iteration domain} of a statement \texttt{S} is the set of iteration vectors whose values can be taken during the execution of the program.
This means that it must respect the constraints of the surrounding for loops and if conditions. It can be represented as an union of polyhedra.

\vspace{0.3cm}
\noindent\emph{Example:} For the statements of the examples introduced previously:
\begin{itemize}
	\item The iteration domain of \texttt{S} is: $\mathcal{D}_S = \left\{ (i,j,k) \mid  0\leq i < N ,~ 0\leq j < N,~ 0 \leq k < N \right\}$.
	\item The iteration domain of \texttt{S1} is: $\mathcal{D}_{S1} = \left\{ (i,k) \mid  0\leq i < N ,~ 0\leq k < i \right\}$.
	\item The iteration domain of \texttt{S2} is: $\mathcal{D}_{S2} = \left\{ (i) \mid  0\leq i < N \right\}$.
\end{itemize}


% == "Typing" of spaces/functions
\paragraph{General advice about the manipulation of mathematical objects in the polyhedral model}
Notice that we will use the same mathematical object to represent different aspects of a program. For example, a polyhedron can be used to represent a set of iterations, a set of memory cell coordinates or a set of timestamps.

% Focus on "typing" of spaces/functions (iteration space, memory, timestamps)
%	=> Figure to emphasis that
In order to avoid errors when combining mathematical objects, we encourage you to check the coherency of the manipulated objects, by keeping track their nature.
For example, if you have a set of memory cell from an array, taking its image by a function that is supposed to take a set of loop iterations as an input is probably a mistake.
Thus, this ``typing'' of the mathematical objects allow you to catch such errors.

% Figure - spaces
\begin{center}
\begin{tikzpicture}
	% Iteration Spaces
	\draw[thick] (1,0) rectangle (5,1.5);
	\node at (3,1.2) {\scriptsize Iteration spaces};

	\node at (2,0.5) {\scriptsize $\mathcal{D}_{S0}$};
	\node at (3,0.5) {\scriptsize $\mathcal{D}_{S1}$};
	\node at (4,0.5) {\scriptsize $\mathcal{D}_{S2}$};

	% Time space
	\draw[thick] (6,0) rectangle (8,1.5);
	\node at (7,1.2) {\scriptsize Time};
	\node at (7,0.5) {\scriptsize $\begin{pmatrix}t_1\\ t_2\end{pmatrix}$};

	% Memory spaces
	\draw[thick] (1,-2.5) rectangle (5,-1);
	\node at (3,-1.3) {\scriptsize Memory spaces};
	\node at (2,-2) {\scriptsize $\mathcal{M}_{temp}$};
	\node at (3,-2) {\scriptsize $\mathcal{M}_{b}$};
	\node at (4,-2) {\scriptsize $\mathcal{M}_{L}$};


	% == Arrows
	\draw[thick,-latex] (5,0.75) -- (6,0.75);
	\node[rotate=90] at (5.5,0) {\scriptsize Schedule};

	\draw[thick,-latex] (3,0) -- (3,-1);
	\node at (1.75,-0.5) {\scriptsize Access functions};
\end{tikzpicture}
\end{center}


% == Access function
\paragraph{Memory space and access function}
A \emph{memory space} of an array is a set of valid memory cell coordinates for this array.
An \emph{access function} is a function that associates the iteration vector of a statement, to a memory cell coordinate that this statement accesses (as a read or a write).

\vspace{0.3cm}
\noindent\emph{Example:} In our running examples:
\begin{itemize}
	\item The memory space of the array $A$ is: $\mathcal{M}_{A} = \left\{ (i,j) \mid 0\leq i < N, ~  0\leq j < N \right\}$.
	\item The memory space of the array $L$ is: $\mathcal{M}_{L} = \left\{ (i,k) \mid  0\leq k \leq i < N \right\}$.
	\item The memory space of the array $b$ is: $\mathcal{M}_{b} = \left\{ (i) \mid  0\leq i < N \right\}$.
	\item The memory space of the variable $temp$ is: $\mathcal{M}_{temp} = \left\{ (0) \right\}$ (it has a single memory cell).

	\vspace{0.3cm}
	\item The access function in statement \texttt{S} that corresponds to the read in \texttt{A} is: $f(i,j,k) = (i,k)$.
	\item The access function in statement \texttt{S} that corresponds to the write in \texttt{C} is: $f(i,j,k) = (i,j)$.
	\item The access function in statement \texttt{S2} that corresponds to the read in \texttt{L} is: $f(i) = (i,i)$.
\end{itemize}

% Linearisation / memory mapping
Notice that the memory space is represented as a multidimensional space.
However, some languages do not allow multidimensional arrays (ex: C).
This means that a multidimensional array is \emph{linearised} into an one-dimensional array.
A (generally)non-affine mapping function allows us to go from its logical multidimensional memory cell coordinates to its real address in the linearised array.

% might have an additional layer here, if the program was put in SSA form => memory mapping to fold the array back, in order to use a reasonnable number of memory cells.
The same idea of ``logical memory coordinates'' arise in some representation, when the program is put in SSA form (for example, in order to differentiate the multiple data stored in the same memory cell during the execution of a program).
In those context, we introduce an additional layer of ``real'' memory space. Then, we introduce \emph{memory mapping} functions, that maps a logical memory cell coordinate to it real memory coordinate.
This allows us to fold the memory, in order to use a reasonable number of memory cells.


\vspace{0.3cm}
\noindent\emph{Example:} The matrix multiplication example can be written in the following way:
\begin{lstlisting}[style=myC]
for (int i=0; i<N; i++)
  for (int j=0; j<N; j++)
    Ctemp[i,j,-1] = 0;
    for (int k=0; k<N; k++)
      Ctemp[i,j,k] = Ctemp[i,j,k-1] + A[i,k] * B[k,j];
    Cout[i,j] = Ctemp[i,j,N-1];
\end{lstlisting}

Notice that the array \texttt{Ctemp} is now 3-dimensional (instead of 2-dimensional), but each memory cells is written exactly once (instead of the accumulation on the old \texttt{C[i,j]}).
Differentiating the different instances of \texttt{Ctemp[i,j,k]} makes it easier to manipulate the way the memory is stored, and corresponds to the logical memory coordinates.

However, when we want to generate our code, using a 3D arrays to store all the values of \texttt{Ctemp} would be a waste, because a 2D array should be enough. Thus, the introduction of a memory mapping $f_{Ctemp}(i,j,k) = (i,j)$ that tells us to fold all the ``logical'' memory cells of \texttt{Ctemp} along the $k$ dimension into a single ``real'' memory cell.


% == Time and schedule
\paragraph{Time and schedule}
In order to represent (and manipulate) the execution order between our statement instance, we represent time as a \emph{multidimensional space of timestamps}.

If we consider sequential programs, we use the \emph{lexicographic order} to order totally the timestamps.
If we consider parallel programs, we pick some dimensions to be parallel, and use the lexicographic order on the other dimensions to have a partial order between the timestamps.

A \emph{schedule} of a statement is a function that associate a iteration vector (of that statement), to a timestamp.


\vspace{0.3cm}
\noindent\emph{Example:} In our running examples:
\begin{itemize}
	\item A possible schedule for statement \texttt{S} is: $\theta_{S}(i,j,k) = (i,j,k)$.
	Note that other schedules (like $\theta_{S}(i,j,k) = (i,2j,k+2)$) are possible, but might not be as compact as the first schedule we proposed.

	\item Possible schedules for:
	\begin{itemize}
		\item statement \texttt{S0} is: $\theta_{S0}(i) = (i,0,0)$
		\item statement \texttt{S1} is: $\theta_{S1}(i,k) = (i,1,k)$.
		\item statement \texttt{S2} is: $\theta_{S2}(i) = (i,2,0)$.
	\end{itemize}
	Indeed, the program is imperfectly nested: for a given iteration $i$, statement \texttt{S0} executes before the loop containing statement \texttt{S1}, which executes before statement \texttt{S2}.
	By adding a \emph{scalar} dimension (whose value is $0$, $1$ or $2$), we encode the textual ordering between these statements.
	The $0$s in the last dimensions of $\theta_{S0}$ and $\theta_{S2}$ are added to have the same number of dimensions in the output space of all scheduling functions.
\end{itemize}


% (2d+1) schedules (for imperfectly nested loops)
For imperfectly nested loops, we can encode in general the textual ordering between loops and statement at the same level, by adding a textual dimension.
Thus, the idea of \emph{$(2d+1)$-schedule}: given a program whose maximal loop depth is $d$, we encode its schedule by alternating scalar dimension (textual ordering between statements/loops) and index dimension.
Notice that the schedule of any imperfectly nested loop program can be written in a $(2d+1)$-schedule manner.

\vspace{0.3cm}
\noindent\emph{Example:} In our second running example, the $(2d+1)$-schedules are:
\begin{itemize}
	\item For statement \texttt{S0}: $\theta_{S0}(i) = (0,i,0,0,0)$.
	\item For statement \texttt{S1}: $\theta_{S1}(i,k) = (0,i,1,k,0)$.
	\item For statement \texttt{S2}: $\theta_{S2}(i) = (0,i,2,0,0)$.
\end{itemize}




% == Extending the class of program that can be represented
\paragraph{Extending the class of program that can be represented}
Notice that even if the program is not completely polyhedral, we might still be able to use polyhedral techniques to analyze and to transform it.

Depending on the situation, there are several tricks that might be applicable to fall back into the polyhedral world.
Here is a short and incomplete list of such tricks:
\begin{itemize}
	\item Replacing a non-affine expression using only parameters (ex: $N\times M$) by a new parameter (ex: $P$)
	\item Over-approximating a set by an polyhedron
	\item Focusing on a band of affine loops, while considering the indexes above it as parameters, and the code below it as a (macro-)statement.
	\item When dealing with indirect array accesses, we can analyzing at compile time the values of an input, and triggering a special case when it follows an affine trend.  % Papier => Apollo
\end{itemize}

Notice that this is not always possible to find/apply such tricks, depending on the kind of work we wish to perform on these program.

% TODO: références ??? (est-ce vraiment pertinent? Est-ce qu'il y a une seule origine à ces concepts?)

\end{document}
