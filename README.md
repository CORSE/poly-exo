# poly-exo

Collection of lessons and learning exercises for the polyhedral model, and organized into knowledge fragments.


## Description

Polyhedral compilation is based on a compact and geometrical program representation, which is used to (typically) optimize the performance of a program.

These analysis and transformations use many mathematical notions and techniques to manipulate them, in order to produce the required information or perform a transformation.

However, there is a lot to unpack at once, and this can be an issue for newcomers, that need to deal with several new concepts at once, when reading the most visible and fundamental papers of the domain.

Therefore, the goal of this repository is to decompose that wall of knowledge into smaller fragments, in order to introduce step by step the different layers of knowledge. This would allow newcomers to build progressively (and more pedagogically) toward the understanding of the key papers.


## Organization of the knowledge nodes

A knowledge node is a group of lesson and associated exercice focus on one or several notions. In practice, each knowledge node is associated with a folder of the repository, which contains a `metadata.yml` file.

The knowledge nodes are classified into 3 layers:
	- Mathematical layer : how to manipulate the mathematical objects and what algorithms/operations can we perform on them
	- Representation layer: how to use these mathematical objects to represent aspects of a program, and how do they work
	- Analysis and transformation layer: how to use the polyhedral representation to extract information or to perform transformation on a program.

The file `metadata.yml` list various information about the content of the folder, in particular where is the lesson, the exercices and their associated correction. Potentially several lessons can be proposed concurrently, in the case of several authors, and exercices can be freely added in a node.



## Graph of knowledge and progression of this repository

TODO: do a serie of script on the content of this repository to have the following infos:
	- script to parse the folders, check the coherence between ids/dependencies
	- script to build the graph of knowledge & output it graphically
	- script to print a slide of the graph, in respect to a list of targeted knowledge



## Contributing

There are two ways you can contribute quickly:
	1) either by adding an exercice (or editing/proposing an alternative version of a lesson) to a knowledge node
	2) adding a new knowledge node in the graph

In both cases, you will need to find/create the corresponding folder and modify the `metadata.yml` to catalog it.

1) The exercice file can be anything that can be read and modified (.txt, .md, .tex, ...). It must be associated to a correction file.

2) If you add a new knowledge, you should be careful about your dependences with the previous knowledge nodes. The graph of knowledge might be useful to get what you need.

The organisation of a metadata.yml file is the following:
```yaml
knowledge: state of the folder
  id : Unique id of the folder, across all the repository
  description: Description of the knowledge
  cat : in which layer is this knowledge (maths, representation OR analysis_transformation)
  type: theory OR tool
  dependencies: list of unique id corresponding to the knowledge required
  complete: is this folder done ? (has a lesson + at least an exercise/correction)

lesson: lesson describing the knowledge fragment. Possible to have several versions
  id : Unique id of the lesson, across the folder
  filename: where its file is (.tex, .txt, .md)
  author: list of author + email (if you wish to be contacted)

exercise: one of the exercise corresponding to the lesson.
  id : Unique id of the exercise, across the folder
  filename: where its file is
  filename_corr: where the correction file is
  author: list of author + email (if you wish to be contacted)
  difficulty: 1 = direct application | 2 = advanced application | 3 = hard
```

## Reference 

// TODO : list the pédagogigal ressources used for inspiration here
// (list of papers used should be at the end of the lessons)
