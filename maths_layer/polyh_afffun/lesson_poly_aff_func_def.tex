\documentclass[a4paper, 11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

%\usepackage{hyperref}
\voffset -1.5cm
\hoffset 0.0cm
\textheight 23cm
\textwidth 16cm
\topmargin 0.0cm
\oddsidemargin 0.0cm
\evensidemargin 0.0cm

\usepackage{epsfig}  
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}

\usepackage{tikz}
\usepackage{array}




\title{Basic definitions - Polyhedron and affine function}
\author{Guillaume Iooss}
\date{}


\begin{document}

\maketitle

This document describes the minimal knowledge about the definition of a polyhedron and an affine function.
These two objects are the most basic mathematical objects that are manipulated in the context of the polyhedral compilation.


% == Affine expression
\paragraph{Affine Expression}

%\begin{block}{Affine Expression}
An \emph{affine expression} is a term of the form $(a_1.i_1 + \dots + a_n.i_n + b)$, where the $a_k$ and $b$ are constant and the $i_k$ are indices.
%\end{block}

We can represent an affine expression as a scalar product: $(\vec{a}.\vec{i} + b)$ where $\vec{a} = (a_1, \dots, a_n)$ and $\vec{i} = (i_1, \dots, i_n)$ are $n$-dimensional vectors.



% == Affine function
\paragraph{Affine function}
An \emph{affine function} is a (potentially multidimensional) function whose expressions are affine:
$$
f(i_1, \dots, i_n) = \left( A_{1,1}.i_1 + \dots + A_{1,n}.i_n + b_1 ~,~ \dots ~,~  A_{m,1}.i_1 + \dots + A_{m,n}.i_n + b_m  \right)
$$
where $n$ is the dimension of the input space of $f$, and $m$ is the dimension of the output space of $f$.
%
We can use a matrix-vector product to have a more compact representation of an affine function:
$$
 f(\vec{i}) = A.\vec{i} + \vec{b}
$$
where $A$ is a $m \times n$ matrix and $b$ is a $m$-dimensional vector of constants.

\vspace{0.3cm}
\noindent \emph{Examples:}
\begin{itemize}
	\item $f_1(i,j) = (i+1, j-2)$ is an affine function.
	\item $f_2(i,j) = (i+1, i-2j)$ is an affine function.
	\item $f_3(i) = (i \times i, i)$ is NOT an affine function, because of the product $i \times i$ which is quadratic.
\end{itemize}

The matrix representation of $f_1$ is:
$$
f_1(i,j) = \begin{bmatrix} 1 & 0\\ 0 & 1\end{bmatrix} . \begin{pmatrix}i\\j\end{pmatrix} + \begin{pmatrix}1\\-2\end{pmatrix}
$$




% == Affine constraint / half-space
\paragraph{Affine constraint}
An affine \emph{half space} is a set of the following form:
$$
	H = \left\{ \vec{i} \mid  \vec{a}.\vec{i} + b \geq 0 \right\}
$$
where $\vec{a}$ is a vector of constants, and $b$ is a constant.
%
$(\vec{a}.\vec{i} + b \geq 0)$ is an \emph{affine constraint}, which is also an inequality.

A compact representation of $c$ affine inequalities is: $A.\vec{i} + \vec{b} \geq \vec{0}$,
where $A$ is a $c \times n$ matrix and $\vec{b}$ is $c$-dimensional vector of constants.

% Equality
We extend the definition of affine constraints to cover \emph{equality affine constraints} ($\vec{a}.\vec{i} + b = 0$). Notice that such constraint can be obtained through the combination of two inequality affine constraints $(\vec{a}.\vec{i} + b \geq 0)$ and $(\vec{a}.\vec{i} + b \leq 0)$.


% == Polyhedron
\paragraph{Polyhedron} A \emph{polyhedron} is a set of points (in $\mathbb{K}^n$) that satisfy several affine constraints:
$$
P = \left\{ \vec{i} \mid A.\vec{i} + \vec{b} \geq \vec{0} \right\} \subset \mathbb{K}^n
$$
where $A$ is a matrix of constant of size $c \times n$, $\vec{b}$ is a vector of constants of size $c$, and $c$ is the number of constraints.

% Equality
As an alternate definition, we can also show explicitly the equality constraints inside a polyhedron:
$$
P = \left\{ \vec{i} \mid A.\vec{i} + \vec{b} \geq \vec{0}, ~ A_{eq}.\vec{i} + \vec{b}_{eq} = \vec{0} \right\} \subset \mathbb{K}^n
$$

% Set of integer points VS set of rational points
About $\mathbb{K}$, we will consider either $\mathbb{K} = \mathbb{Q}$ or $\mathbb{K} = \mathbb{Z}$:
\begin{itemize}
	\item When $\mathbb{K} = \mathbb{Q}$, a polyhedron has more clement mathematical properties, for example in respect to convexity, or its dual representation.
	\item $\mathbb{K} = \mathbb{Z}$ is the case that interests us the most. It can be used to represent discrete notions of a program, such as an iteration space. In that case, a polyhedron is sometimes called a \emph{$\mathbb{Z}$-polyhedron}.
\end{itemize}
In the rest of these lessons, we will specify the kind of polyhedron we consider, when it is critical.

% Polytope
A polyhedron which is bounded is called a \emph{polytope}.

\vspace{0.3cm}
\noindent \emph{Examples:}
\begin{itemize}
	\item $P_1 = \left\{ (i,j) \mid i \geq 0 ,~ j \geq 0 \right\}$ is a polyhedron.
	\item $P_2 = \left\{ (i,j,k) \mid i \geq 2,~ k + 2 \leq 0 \right\}$ is a polyhedron.
	\item $P_3 = \left\{ (i,j) \mid i \times j \geq 0 \right\}$ is NOT a polyhedron, due to its quadratic constraint. However, this set could be actually expressed as the union of 2 polyhedra (one for $i,j\geq 0$ and one for $i,j \leq 0$).
	\item $P_4 = \left\{ (i,j,k) \mid 0 \leq i=j=k < 100 \right\}$ is a polyhedron.
\end{itemize}





%Depending on the context, we will consider polyhedron of integer points or of rational points, i.e., $\mathbb{K} = \mathbb{Z}$ or $\mathbb{K} = \mathbb{Q}$ in the previous definitions.
%Using a polyhedron of integer points (sometimes called a $\mathbb{Z}$-polyhedron) is useful for representing discrete notions of a program, such as iteration space.
%A polyhedron of rational point has interesting mathematical properties, for example in respect to convexity or its dual representation.


% == Parameters
\paragraph{Parameters}
A \emph{parameter} is a symbolic constant. It can be used to parametrize the affine expression of an affine function, or the constraints of a polyhedron. The value of a parameter is unknown, but it can be constrained by affine constraints.

In practice, a parameter is often used to represent a constant of a program, such as a size of an array.

The definition of polyhedron becomes:
$$
P = \left\{ \vec{i} \mid A.\vec{i} + A_{p}.\vec{p} + \vec{b} \geq \vec{0}, A_{eq}.\vec{i} + A_{eq,p}.\vec{p} + \vec{b}_{eq} = \vec{0} \right\} \subset \mathbb{K}^n
$$
where $\vec{p}$ are the parameters. Likewise, the definition of an affine function becomes:
$$
f(\vec{i}) = A.\vec{i} + A_{p}.\vec{p} + \vec{b}
$$

A variant of this definition combines $\vec{p}$ with $\vec{i}$. For example, for the affine function:
$$
f(\vec{i}) = A.\begin{pmatrix}\vec{i}\\\vec{p}\end{pmatrix} + \vec{b}
$$



\vspace{0.3cm}
\noindent \emph{Examples:}
\begin{itemize}
	\item $P_5 = \left\{ (i,j) \mid 0 \leq i < N,~ 0 \leq j < N,~ N \geq 0 \right\}$ is a polyhedron, which is a square of size $N$, where $N$ is a parameter. Notice that $N \geq 0$ is an affine constraint on its parameter.
	\item $P_6 = \left\{ (i,j) \mid 1 \leq i ,~ -1 \leq j, i+j \leq N, j \leq M \right\}$ is a polyhedron where $N$ and $M$ are parameters. Depending on the values of $N$ and $M$, it can be either a triangle or a trapezoid.
\end{itemize}

The compact representation of $P_5$ is:
$$
P_5 = \left\{ \begin{pmatrix}i\\j\end{pmatrix} \mid \begin{bmatrix}0&1&0\\ 1&-1&0\\ 0&0&1\\ 1&0&-1\\ 1&0&0\end{bmatrix} . \begin{pmatrix}N\\i\\j\end{pmatrix} + \begin{pmatrix}0\\-1\\0\\-1\\0\end{pmatrix} \geq \begin{pmatrix}0\\0\\0\\0\\0\end{pmatrix} \right\}
$$





% Figure for the last polyhedron (P_6)
Here is a graphical representation of $P_6$, and, in particular, of its affine constraints:
\begin{center}
\begin{tikzpicture}
	% Axis
	\draw[thick,-latex] (-0.2,0) -- (4.3,0);
	\node at (4.2,-0.3) {$i$};

	\draw[thick,-latex] (0,-0.7) -- (0,4.3);
	\node at (-0.3,4.2) {$j$};

	% Constraints
	\draw[thick,dashed,red] (0.5,-0.6) -- (0.5,4.2);
	\draw[-latex,red] (0.5,0.5) -- (0.9,0.5);
	\node[red] at (1,3.8) {\scriptsize $1 \leq i$};

	\draw[thick,dashed,blue] (-0.2,-0.5) -- (4.2,-0.5);
	\draw[-latex,blue] (1.7,-0.5) -- (1.7,-0.1);
	\node[blue] at (2.5,-0.8) {\scriptsize $-1 \leq j$};

	\draw[thick,dashed,orange] (-0.2,1.5) -- (4.2,1.5);
	\draw[-latex,orange] (1.7,1.5) -- (1.7,1.1);
	\node[orange] at (3.5,1.7) {\scriptsize $j \leq M$};

	\draw[thick,dashed,teal] (-0.2,3.7) -- (4.2,-0.7);
	\draw[-latex,teal] (2.8,0.7) -- (2.5,0.4);
	\node[teal] at (3.5,0.8) {\scriptsize $i + j \leq N$};
	

	% Ticks (every 0.5)
	\draw[thick] (0.5,0.1) -- (0.5,-0.1);
	\draw[thick] (1,0.1) -- (1,-0.1);
	\draw[thick] (1.5,0.1) -- (1.5,-0.1);
	\draw[thick] (2,0.1) -- (2,-0.1);
	\draw[thick] (2.5,0.1) -- (2.5,-0.1);
	\draw[thick] (3,0.1) -- (3,-0.1);
	\draw[thick] (3.5,0.1) -- (3.5,-0.1);
	\draw[thick] (4,0.1) -- (4,-0.1);

	\draw[thick] (-0.1,-0.5) -- (0.1,-0.5);
	\draw[thick] (-0.1,0.5) -- (0.1,0.5);
	\draw[thick] (-0.1,1) -- (0.1,1);
	\draw[thick] (-0.1,1.5) -- (0.1,1.5);
	\draw[thick] (-0.1,2) -- (0.1,2);
	\draw[thick] (-0.1,2.5) -- (0.1,2.5);
	\draw[thick] (-0.1,3) -- (0.1,3);
	\draw[thick] (-0.1,3.5) -- (0.1,3.5);
	\draw[thick] (-0.1,4) -- (0.1,4);

	% Legend
	\node at (2.2,-1.3) {\scriptsize \textbf{Graphical representation of $P_6$}};
\end{tikzpicture}
\end{center}



% TODO Ref needed?

\end{document}

