
# Correction of ex_poly_aff_func_Gui.tex

## Exercise 1

a) 4: No, Rest: Yes
b) 3: No, Rest: Yes


## Exercise 2

1) f_1(i,j,k) = A . (i,j,k) + b 
where:
    [0 1 0]          [0]
A = [1 0 1]  and b = [1]
    [0 1 0]          [0]


2) f_2(i,j) = A . (i,j) + b 
where:
    [1 0]          [ 1]
A = [0 1]  and b = [-1]

3) P = { i,j,k | A . (i,j,k) + A_p . N + b >= 0  &&  C.(i,j,k) + C_p . N + d = 0 }
where:
   [ 1  0  0]      [0]       [ 0]
   [-1  0  0]      [1]       [-1]
   [ 0  1  0]      [0]       [ 0]
A= [ 0 -1  0], A_p=[1] and b=[-1]
   [ 0  0  1]      [0]       [ 0]
   [ 0  0 -1]      [1]       [-1]

C= [ 2  1  0], C_p=[0] and d=[-1]


4) P = { i,j,k | A . (i,j,k) + A_p . N + b >= 0 }
where:
   [ 1  0  0]      [0]       [ 0]
   [-1  1  0]      [0]       [ 0]
A= [ 0 -1  1], A_p=[0] and b=[ 0]
   [ 0  0 -1]      [1]       [-1]



## Exercise 3

a) P_1 = { i,j | 0<=j , i<=N , i<=j }
b) P_2 = { i,j | 0<=i , i<=N , 0<=j , j<=N+M, 2<=i+j }


