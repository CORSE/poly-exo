\documentclass[a4paper, 11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

%\usepackage{hyperref}
\voffset -1.5cm
\hoffset 0.0cm
\textheight 23cm
\textwidth 16cm
\topmargin 0.0cm
\oddsidemargin 0.0cm
\evensidemargin 0.0cm

\usepackage{epsfig}  
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}

% For the colors
\usepackage[dvipsnames]{xcolor}

\usepackage{tikz}
\usepackage{array}


\title{Basic operations on polyhedron and affine function}
\author{Guillaume Iooss}
\date{}


\begin{document}

\maketitle

% Summary of content
This document introduces the basic operations that involves polyhedra and affine functions:
\begin{itemize}
	\item Intersection between two polyhedra: $\mathcal{P}_1 \cap \mathcal{P}_2$
	\item Complementary of a polyhedra: $\neg \mathcal{P}$
	\item Difference between two polyhedra: $\mathcal{P}_1 \backslash \mathcal{P}_2$
	\item Composition of two affine functions: $f_1 \circ f_2$
	\item Preimage of a polyhedron by an affine function: $f^{-1}(\mathcal{P})$
\end{itemize}

Concerning the image of a polyhedron by an affine function ($f(\mathcal{P})$), we will describe the case where the affine function is \emph{unimodular}.
Finally, we will introduce the \emph{Fourier-Motzkin} algorithm, which is one way to cover the case where the function is a canonical projection, and the polyhedron is over rational points.


% Naming convention
\paragraph{Naming conventions} While describing these operations, we will use the following notations to describe our starting polyhedra and affine functions:
$$
	\mathcal{P}_1 = \left\{ \vec{i} \mid  A.\vec{i}+\vec{b} \geq \vec{0} \right\}, \quad \mathcal{P}_2 = \left\{ \vec{i} \mid  C.\vec{i}+\vec{d} \geq \vec{0} \right\}
$$
and
$$
	f_1: \vec{i} \mapsto F_1.\vec{i} + \vec{f}_1 , \quad f_2: \vec{i} \mapsto F_2.\vec{i} + \vec{f}_2
$$



% == Intersection
\paragraph{Intersection of two polyhedra} The intersection of two polyhedra is always a polyhedra. The constraints of the resulting polyhedron are:
$$
	\mathcal{P}_1 \cap \mathcal{P}_2 = \left\{ \vec{i} \mid  \left(\begin{array}{c}A\\C\end{array}\right) . \vec{i} + \left(\begin{array}{c}\vec{b}\\\vec{d}\end{array}\right) \geq \vec{0} \right\}
$$

\vspace{0.3cm}
\noindent \emph{Examples:}
With $\mathcal{P}_1 = \left\{ (i,j) \mid 0\leq i \leq j < N \right\}$, $\mathcal{P}_2 = \left\{ (i,j) \mid 0\leq i< M \right\}$ and $\mathcal{P}_3 = \left\{ (i,j) \mid i=j+1 \right\}$:
\begin{itemize}
	\item $\mathcal{P}_1 \cap \mathcal{P}_2 = \left\{ (i,j) \mid 0\leq i \leq j < N , i \leq M \right\}$.
	\item $\mathcal{P}_1 \cap \mathcal{P}_3 = \left\{ (i,j) \mid 0\leq i \leq j < N, i=j+1 \right\} = \emptyset$.
	\item $\mathcal{P}_2 \cap \mathcal{P}_3 = \left\{ (i,j) \mid 0\leq i< M, j=i-1 \right\}$.
\end{itemize}



% == Difference
\paragraph{Difference between two polyhedra} We can compute the difference between two polyhedra ($\mathcal{P}_1 \backslash \mathcal{P}_2$) by computing the intersection $\mathcal{P}_1 \cap (\neg \mathcal{P}_2)$, where $(\neg \mathcal{P})$ is the complementary of the polyhedron $\mathcal{P}$.

The complementary of a polyhedron $\mathcal{P}$ is a union of polyhedra:
$$
\begin{array}{rl}
	\neg \mathcal{P} =& \left\{ \vec{i} \mid \vec{A}_{1, \bullet}.\vec{i} + b_1 < 0 \right\}\\
	 \uplus & \left\{ \vec{i} \mid \vec{A}_{1, \bullet}.\vec{i} + b_1 \geq 0 ,~ \vec{A}_{2, \bullet}.\vec{i} + b_2 < 0 \right\}\\
	 \uplus & \dots\\
	 \uplus & \left\{ \vec{i} \mid \vec{A}_{1, \bullet}.\vec{i} + b_1 \geq 0 ,~ \dots ~,~ \vec{A}_{c-1, \bullet}.\vec{i} + b_{c-1} \geq 0,~ \vec{A}_{c, \bullet}.\vec{i} + b_{c} < 0 \right\}
\end{array}
$$
where $\vec{A}_{i,\bullet}$ is the ith row vector of the matrix $A$, and $c$ is the number of rows of $A$ (the number of constraints of the polyhedron).

Therefore, the difference between two polyhedra is also a union of polyhedra.


\vspace{0.3cm}
\noindent \emph{Example:} With $\mathcal{P}_1 = \left\{ (i,j) \mid 0\leq i, ~ 0\leq j, ~  i+j \leq N \right\}$ and $\mathcal{P}_2 = \left\{ (i,j) \mid 0\leq i<M, ~ 0\leq j < M \right\}$:
\begin{itemize}
	\item The complementary of $\mathcal{P}_2$ is:
	\begin{displaymath}
	\begin{array}{rl}
		\neg \mathcal{P}_2 =& \textcolor{red}{\left\{ (i,j) \mid i < 0 \right\}}  \uplus  \textcolor{BurntOrange}{\left\{ (i,j) \mid 0 \leq i, ~ M \leq i \right\}} \uplus \textcolor{blue}{\left\{ (i,j) \mid 0\leq i<M, ~ j < 0 \right\}}\\
		& \uplus \textcolor{ForestGreen}{\left\{ (i,j) \mid 0 \leq i <M, ~ 0 \leq j , ~ M \leq j \right\}}
	\end{array}
	\end{displaymath}
	Here is a graphical representation of this union of polyhedra:
\begin{center}
\begin{tikzpicture}

	% Constraints
	\draw[fill,style=dotted,red,fill=red!20] (-2.1,-2.2) rectangle (0,2.2);
	\draw[fill,style=dotted,red,fill=BurntOrange!20] (1,-2.2) rectangle (2.2,2.2);
	\draw[fill,style=dotted,red,fill=blue!20] (0,-2.2) rectangle (1,0);
	\draw[fill,style=dotted,red,fill=ForestGreen!20] (0,1) rectangle (1,2.2);

	% Axis
	\draw[thick,-latex] (-2.2,0) -- (2.3,0);
	\node at (2.2,-0.3) {$i$};

	\draw[thick,-latex] (0,-2.3) -- (0,2.3);
	\node at (-0.3,2.2) {$j$};

	% Ticks (every 0.5)
	\draw[thick] (-2,0.1) -- (-2,-0.1);
	\draw[thick] (-1.5,0.1) -- (-1.5,-0.1);
	\draw[thick] (-1,0.1) -- (-1,-0.1);
	\draw[thick] (-0.5,0.1) -- (-0.5,-0.1);
	\draw[thick] (0.5,0.1) -- (0.5,-0.1);
	\draw[thick] (1,0.1) -- (1,-0.1);
	\draw[thick] (1.5,0.1) -- (1.5,-0.1);
	\draw[thick] (2,0.1) -- (2,-0.1);

	\draw[thick] (-0.1,-2) -- (0.1,-2);
	\draw[thick] (-0.1,-1.5) -- (0.1,-1.5);
	\draw[thick] (-0.1,-1) -- (0.1,-1);
	\draw[thick] (-0.1,-0.5) -- (0.1,-0.5);
	\draw[thick] (-0.1,0.5) -- (0.1,0.5);
	\draw[thick] (-0.1,1) -- (0.1,1);
	\draw[thick] (-0.1,1.5) -- (0.1,1.5);
	\draw[thick] (-0.1,2) -- (0.1,2);

	% Shape
	\draw (0,0) rectangle (1,1);
	\node at (0.5,0.5) {$\mathcal{P}_2$};
	
	% Legend
	\node at (0,-2.8) {\scriptsize \textbf{Graphical representation of $\neg \mathcal{P}_2$}};
\end{tikzpicture}
\end{center}

	\item Thus, the difference $\mathcal{P}_1 \backslash \mathcal{P}_2$ is:
	\begin{displaymath}
	\begin{array}{rl}
		\mathcal{P}_1 \backslash \mathcal{P}_2 =& \emptyset  \uplus  \left\{ (i,j) \mid 0 \leq i, ~ M \leq i, ~ 0 \leq j , ~ i+j \leq N \right\} \uplus \emptyset \\
		& \uplus \left\{ (i,j) \mid 0 \leq i <M, ~ 0 \leq j , ~ M \leq j , ~ i+j \leq N \right\}
	\end{array}
	\end{displaymath}
\end{itemize}



% == Composition of function
\paragraph{Composition of affine function} The composition of two affine functions is an affine function:
$$
	f_1 \circ f_2 : \vec{i} \mapsto F_1 (F_2 . \vec{i} + \vec{f}_2) + \vec{f}_1 = F_1.F_2.\vec{i} + (F_1.\vec{f}_2 + \vec{f}_1)
$$

\vspace{0.3cm}
\noindent \emph{Examples:} With $f_1(i,j) = (j,i)$ and $f_2(i,j) = (i+1,j-1)$, $(f_1 \circ f_2)(i,j) = (j-1, i+1)$.


% == Preimage
\newpage
\paragraph{Preimage} The preimage of a polyhedron by an affine function is a polyhedron:
$$
	\begin{array}{rl}
	f^{-1}(\mathcal{P}) =& \{ \vec{i} \mid f(\vec{i}) \in \mathcal{P} \}\\
	 =& \left\{ \vec{i} \mid A.(F.\vec{i} + \vec{f}) + \vec{b} \geq 0 \right\}
	\end{array}
$$

\vspace{0.3cm}
\noindent \emph{Example:} With $f(i,j)=(i+1,j+1)$ and $\mathcal{P} = \left\{ (i,j) \mid 0\leq i, ~ 0\leq j, ~ i+j\leq N \right\}$:
$$
	f^{-1}(\mathcal{P}) = \left\{ (i,j) \mid 0\leq i+1, ~ 0\leq j+1, ~ i+j+2\leq N \right\}
$$



% == Image - general
\paragraph{Image of a polyhedron by an affine function} Taking the image of a polyhedron $f(\mathcal{P})$ is more complicated, and depends if $\mathcal{P} \in \mathbb{Q}^n$ or $\mathcal{P} \in \mathbb{Z}^n$.

\vspace{0.3cm}
\noindent \underline{\emph{If the polyhedron lives in $\mathbb{Z}^n$ (and the affine function has only integral coefficient):}} an image might introduce a stride.
%
For example, if we consider $\mathcal{P} = \left\{ i \in \mathbb{Z} \mid 0 \leq i < N \right\}$ and $f(i) = 2i$:
$$
	f(\mathcal{P}) = \left\{ i \in \mathbb{Z} \mid (\exists k \in \mathbb{Z}) ~ 0 \leq k < N, ~ i = 2k \right\}
$$
which is the set of all the points of even coordinate between $0$ and $(2N-2)$ included.
This new set contains an \emph{existential dimension}, and is no longer a polyhedron (but a \emph{Presburger set}).

One special case happens when the affine function $f(\vec{i}) = U.\vec{i} + \vec{b}$ is \emph{unimodular}, which means that its determinant $det(U) = \pm 1$.
Indeed, in this situation, $U^{-1}$ is a matrix with integral coefficients, so the inverse $f^{-1}$ also has integral coefficients ($f^{-1}(\vec{i}') = U^{-1}.(\vec{i}' -\vec{b})$).
Therefore, $f(\mathcal{P})$ is still an polyhedron living in $\mathbb{Z}^n$.


\vspace{0.3cm}
\noindent \emph{Example:} With $f(i,j) = (2i+j, i+j)$ and $\mathcal{P} = \left\{ (i,j) \mid 0\leq i, ~ 0\leq j, ~ i+j\leq N \right\}$:
\begin{itemize}
	\item $f(i,j) = \begin{bmatrix}2&1\\ 1&1\end{bmatrix}.\begin{pmatrix}i\\j\end{pmatrix}$, and $det\left(\begin{bmatrix}2&1\\ 1&1\end{bmatrix}\right) = 1$. So, $f$ is unimodular.
	\item After computing its inverse, we obtain: $f^{-1}(i', j') = \begin{bmatrix}1&-1\\ -1&2\end{bmatrix}.\begin{pmatrix}i'\\j'\end{pmatrix}$
	\item Therefore, $f(\mathcal{P}) = (f^{-1})^{-1}(\mathcal{P}) = \left\{ (i',j') \mid 0\leq i'-j', ~ 0\leq 2j'-i', ~ j'\leq N \right\}$
\end{itemize}




\vspace{0.3cm}
\noindent \underline{\emph{If the polyhedron lives in $\mathbb{Q}^n$:}} the image is a polyhedron.
%
This can be deduced by considering the \emph{Smith Normal Form} of the matrix of the affine function: $F = U.S.V$ where $U$ and $V$ are unimodular matrices, and $S = diag(s_1, \dots, s_k, 0, \dots, 0)$ is a diagonal matrix whose first elements ($s_i$) are strictly positive.

Therefore, in order to cover the general case, we can focus on two special cases:
\begin{itemize}
	\item The image through an unimodular function
	\item The image through a canonical projection
\end{itemize}

The case of an unimodular function is identical to the one seen when $\mathcal{P} \in \mathbb{Z}^n$.


% === OLD ===
\iffalse
	% == Image - unimodular case
	\paragraph{Image of a polyhedron - unimodular case}
	Taking the image of a polyhedron is much more complicated, thus let us consider first a simpler case.

	An affine function $f(\vec{i}) = U.\vec{i} + \vec{b}$ is said to be \emph{unimodular} when $|det(U)| = 1$.
	An unimodular function $U$ is always invertible, and if its coefficients are integers, then its inverse $U^{-1}$ also have integer coefficients.

	Therefore, if we consider the image of a polyhedron by an unimodular function, we have:
	$$
	\begin{array}{rl}
		f(\mathcal{P}_1) & = \left\{ \vec{j} \mid \vec{j} = U.\vec{i}+\vec{b} ,  A.\vec{i} + \vec{b} \geq 0 \right\}\\
			& = \left\{ \vec{j} \mid A.U^{-1}.(\vec{j}-\vec{b}) + \vec{b} \geq 0 \right\}
	\end{array}
	$$
\fi
% === END OLD ===



% == Image - projection
\paragraph{Image of a polyhedron by a canonical projection}
Let us consider a polyhedron $\mathcal{P} \in \mathbb{Q}^n$ and a canonical projection: $f(\vec{i_1},\vec{i_2}) = \vec{i_1}$.
We can compute the image $f(\mathcal{P})$ by using the \emph{Fourier-Motzkin elimination method}.

% Fourier-Motzkin
This algorithm works in the following way. While there are still dimensions to be projected:
\begin{itemize}
	\item Let us consider a dimension $i \in \vec{i_2}$ that we wish to eliminate. Let us call $\vec{i'}$ the rest of the dimensions.
	\item We partition the constraints of $\mathcal{P}$ into 3 sets of constraints:
	\begin{itemize}
		\item $\mathcal{S}_1$: The constraints in which the coefficient in front of $i$ is positive, and which can be written as $(i \geq a_k.\vec{i'} + b_k)$.
		\item $\mathcal{S}_2$: The constraints in which the coefficient in front of $i$ is negative, and which can be written as $(i \leq c_k.\vec{i'} + d_k)$
		\item $\mathcal{S}_3$: The constraints in which $i$ is not involved.
	\end{itemize}

	\item For every couple of constraints $(i \geq a_k.\vec{i'} + b_k) \in \mathcal{S}_1$, and $(i \leq c_k.\vec{i'} + d_k) \in \mathcal{S}_2$, we can combine them in order to eliminate $i$:
	$$
		a_k.\vec{i'} + b_k \leq i \leq c_k.\vec{i'} + d_k
	$$
	By doing so, we obtain $card(\mathcal{S}_1) \times card(\mathcal{S}_2)$ new constraints $\mathcal{S}_{12}$ which does not involve $i$.

	\item The set of projected constraints on dimensions $\vec{i'}$ are $\mathcal{S}_{12} \cup \mathcal{S}_3$.
\end{itemize}

This algorithm is also useful to determine if a polyhedron is empty.

However, notice that this algorithm can generate an exponential number of constraints, in the worst case, due to the large amount of constraints it generates for each projected dimension.


\vspace{0.3cm}
\noindent \emph{Example:} With $f(i,j)=(i)$ and $\mathcal{P} = \left\{ (i,j) \mid 0 \leq j \leq N, ~ j \leq i \leq M+j, ~ 0 \leq N \right\}$:
\begin{itemize}
	\item We wish to eliminate the $j$ dimension.
	\item We partition the constraints into 3 sets:
	\begin{itemize}
		\item $\mathcal{S}_1$ : $0 \leq j$ and $i-M \leq j$.
		\item $\mathcal{S}_2$ : $j \leq N$ and $j \leq i$.
		\item $\mathcal{S}_3$ : $0 \leq N$.
	\end{itemize}

	\item By combining the constraints from the two first sets, we have 4 combinations:
	\begin{itemize}
		\item $0 \leq j \leq N$, which gives us $0 \leq N$
		\item $0 \leq j \leq i$, which gives us $0 \leq i$
		\item $i-M \leq j \leq N$, which gives us $i \leq N+M$
		\item $i-M \leq j \leq i$, which gives us $0 \leq M$
	\end{itemize}
	\item Thus, the projected polyhedron is $f(\mathcal{P}) = \left\{ i \mid 0 \leq i \leq N+M, ~ 0 \leq N ~ 0 \leq M \right\}$
\end{itemize}


\begin{center}
\begin{tikzpicture}
	% Axis
	\draw[thick,-latex] (-0.2,0) -- (4.3,0);
	\node at (4.2,-0.3) {$i$};

	\draw[thick,-latex] (0,-0.3) -- (0,3.3);
	\node at (-0.3,3.2) {$j$};

	% Ticks (every 0.5)
	\draw[thick] (0.5,0.1) -- (0.5,-0.1);
	\draw[thick] (1,0.1) -- (1,-0.1);
	\draw[thick] (1.5,0.1) -- (1.5,-0.1);
	\draw[thick] (2,0.1) -- (2,-0.1);
	\draw[thick] (2.5,0.1) -- (2.5,-0.1);
	\draw[thick] (3,0.1) -- (3,-0.1);
	\draw[thick] (3.5,0.1) -- (3.5,-0.1);
	\draw[thick] (4,0.1) -- (4,-0.1);

	\draw[thick] (-0.1,0.5) -- (0.1,0.5);
	\draw[thick] (-0.1,1) -- (0.1,1);
	\draw[thick] (-0.1,1.5) -- (0.1,1.5);
	\draw[thick] (-0.1,2) -- (0.1,2);
	\draw[thick] (-0.1,2.5) -- (0.1,2.5);
	\draw[thick] (-0.1,3) -- (0.1,3);

	% Shape
	\draw[fill, red] (0,-0.05) rectangle (4,0.05);
	\draw (0,0) -- (1.5,1.5) -- (4,1.5) -- (2.5,0) -- (0,0);
	\node at (2,0.75) {$\mathcal{P}$};
	\node[red] at (2,-0.4) {$f(\mathcal{P})$};

	% Legend
	\node at (2,-1) {\scriptsize \textbf{Projection of $\mathcal{P}$ along dimension $j$}};
\end{tikzpicture}
\end{center}



% ============================================================================================
\paragraph{References}
% Référence: Schrijver / Chap 12.2 (page 155) pour Fourier-Motzkin
% Chap 4.4 (page 50) pour Smith Normal Form + Papier original de 1861
% Référence projection integral case => Cf Seghir/Loechner (permet de faire le cas général)
\begin{itemize}
	\item \emph{Reference book:} "Theory of linear and integer programming", by Alexander Schrijver, Wiley-Interscience series in Discrete Mathematics and Optimization, 1986.

	\item \emph{Smith Normal Form:} description found in Section 4.4 (page 50) of the Schrijver book. Original paper from [Smith 1861].

	\item \emph{Fourier-Motzkin elimination:} description found in Section 12.2 (page 155) of the Schrijver book. Original paper from [Fourier 1827], [Dines 1918-9] and [Motzkin 1936].

	\item (To go further) Image of a polyhedron though a projection, for the $\mathbb{Z}^n$ case:
	\begin{itemize}
		\item "Integer Affine Transformations of Parametric $\mathbb{Z}$-Polytopes and Applications to Loop Nest Optimization", by Seghir, Rachid and Loechner, Vincent and Meister, Benoît. June 2012, ACM Transactions on Architecture and Code Optimisation (ACM TACO). Link: \url{https://hal.inria.fr/inria-00582388}.
	\end{itemize}
\end{itemize}

\end{document}

