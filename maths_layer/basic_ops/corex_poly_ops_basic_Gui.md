
# Correction of ex_poly_ops_basic_Gui.tex


## Exercise 1

1) P_1 \inter P_2 = { i | i <= 2 , -1 < i < N }
2) P_1 \inter P_2 = { i,j,k | 2 <= i <= j+1 <= k , i+j < k, 0 <= k < N }


## Exercice 2

1) P_1 \ P_2 = { i,j | 0<=i , 0<=j , i>j }

2) P_1 \ P_2 = \emptyset U \emptyset
				U { i,j | 0 <= i < N , 0 <= j <= i < N , i+j >= N }

3) P_1 \ P_2 = { i,j | 0 <= i < N , 0 <= j < N , i > j }
				U { i,j | 0 <= i < N , 0 <= j < N , i < j }


## Exercice 3

1) f^{-1}(P) = { i,j | -2<=i , 1 <= j }

2) f^{-1}(P) = { i,j | 0 <= i <= j < N }

3) f^{-1}(P) = { i,j | 0<= i-j , 0<= i+j , (i-j)+(i+j)< N }
			 = { i,j | j <= i , 0<= i+j , 2i < N }


## Exercice 4

1) Determinant = 1 * 1 - 0 * 1 = 1. f is unimodular.
	f^{-1}(i',j') = (i', j'-i)
	f(P) = { i',j' | 0 <= i' < N, 0<= j'-i' < M }

2) Determinant = 0 * 0 - 1 * 1 = -1. f is unimodular.
	f^{-1}(i',j') = (j'-1, i'-1)
	f(P) = { i',j' | 0 <= j'-1 < N, 0<= i'-1 < M }

3) Determinant = 3 * 3 - 4 - 2 = 1. f is unimodular.
	f^{-1}(i',j') = (3i'-4j'+10, -2i'+3j'-7)
	f(P) = { i',j' | 0 <= 3i'-4j'+10 < N, 0<= -2i'+3j'-7 < M }


## Exercice 5

1) Projection along the j dimension:
    	i-N < j <= i
    	0  <= j < M
    So:
    	i-N < i
    	i-N < M
    	0  <= i
    	0   < M
    f(P) = { i | 0 < N, 0 < M, 0 <= i < N+M }

2) (i) Projection along the k dimension: (we can start by either j or k)
		i-j-N1 < k <= i-j
		-j    <= k < N2-j
		0     <= k < N3
	So:
		i-j-N1 < i-j
		i-j-N1 < N2-j
		i-j-N1 < N3
		-j    <= i-j
		-j     < N2-j
		-j     < N3
		0     <= i-j
		0      < N2-j
		0      < N3
	After the projection on k: Pij = { i,j | 0 < N1, 0 < N2, 0 < N3, 0 <= i < N1+N2, 0 <= i-j < N1+N3, -N3 < j < N2}

	(ii) Projection along the j dimension:
		i-N1-N3 < j <= i
		-N3     < j < N2
	So:
		i-N1-N3 < i
		i-N1-N3 < N2
		-N3     < i
		-N3     < N2

	Neutral contraints: 0 < N1, 0 < N2, 0 < N3, 0 <= i < N1+N2

	f(P) = { i | 0 < N1, 0 < N2, 0 < N3, 0 <= i < N1+N2,
					(0 < N1+N3), (0 < N2+N3), (-N3 < i), (i < N1+N2+N3) }
		 = { i | 0 < N1, 0 < N2, 0 < N3, 0 <= i < N1+N2 }


